package tutorial.webapp

import org.scalajs.dom
import org.scalajs.dom.document

object TutorialApp {
  def main(args: Array[String]): Unit = {
    val button = document.createElement("button")
    button.textContent = "Click me"
    button.addEventListener("click", (e: dom.MouseEvent) => println("clicked"))
    document.body.appendChild(button)
  }
}
