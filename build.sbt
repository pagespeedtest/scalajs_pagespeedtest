enablePlugins(ScalaJSPlugin)

name := "scalajs_pagespeedtest"

version := "0.1"

scalaVersion := "2.13.6"

scalaJSUseMainModuleInitializer := true

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.1.0"
